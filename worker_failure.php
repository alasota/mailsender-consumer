<?php
require __DIR__.'/vendor/autoload.php';

use Predis\Async\Client as PredisAsync;

$client = new PredisAsync('tcp://127.0.0.1:6379');


$client->connect(function ($client) {
    echo "Connected to Redis, now listening for incoming messages...\n";

    $consumer = new PredisAsync('tcp://127.0.0.1:6379', $client->getEventLoop());
    $producer = new PredisAsync('tcp://127.0.0.1:6379', $client->getEventLoop());

    //loop for listening queue
    $client->subscribe('mail-in-progress-listening', function ($event, $client) use ($consumer, $producer) {

        $consumer->brpop('mail-in-progress-queue', 0, function ($result, $client) use ($producer) {

            list($key, $message) = $result;

            //time for send mail - timeout
            sleep(5);

            //check if mail has been sent
            $client->sismember('mail-sended', $message, function($result, $client) use ($message, $producer) {
                if($result == false) {
                    $producer->lpush('mail-queue', $message, function($result, $client) use ($message){
                        echo "renew send message: \n" . $message . "\n";
                    });
                }
            });

            //loop
            $client->publish('mail-in-progress-listening', true, function() {});
        });
    });

    $client->publish('mail-in-progress-listening', true, function() {});

});

$client->getEventLoop()->run();