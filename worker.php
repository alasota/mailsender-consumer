<?php
require __DIR__.'/vendor/autoload.php';

use Predis\Async\Client as PredisAsync;

$client = new PredisAsync('tcp://127.0.0.1:6379');


$client->connect(function ($client) {
    echo "Connected to Redis, now listening for incoming messages...\n";

    $consumer = new PredisAsync('tcp://127.0.0.1:6379', $client->getEventLoop());

    //loop for listening queue
    $client->subscribe('mail-queue-listening', function ($event, $client) use ($consumer) {

        $consumer->brpoplpush('mail-queue', 'mail-in-progress-queue', 0, function ($message, $client)  {

            echo "sending message: \n" . $message . "\n";

            try {
                //send message
                $data = unserialize($message);
                $transport = Swift_SmtpTransport::newInstance();
                $mailer = Swift_Mailer::newInstance($transport);

                $mailMessage = Swift_Message::newInstance($data['subject'])
                    ->setFrom(array('test@test.com' => 'Test'))
                    ->setTo(is_array($data['to'])?$data['to']:array($data['to']))
                    ->setCc((is_array($data['cc']))?$data['cc']:array($data['cc']))
                    ->setBody($data['message']);

                $mailer->send($mailMessage);

                //control job in queue
                $client->lrem('mail-in-progress-queue', 1, $message);
                $client->sadd('mail-sended', $message);

                echo date('Y-m-d H:i:s');
                echo "sended\n";

            } catch (\Exception $e) {
                echo $e . "\n";
            }

            //loop
            $client->publish('mail-queue-listening', true, function() {});
        });

    });

    $client->publish('mail-queue-listening', true, function() {});

});

$client->getEventLoop()->run();

